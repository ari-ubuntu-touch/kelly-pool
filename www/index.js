

let balls = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
let playersballs = {};


function shuffle(array) {
  let currentIndex = array.length,  randomIndex;

  // While there remain elements to shuffle.
  while (currentIndex != 0) {

    // Pick a remaining element.
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex], array[currentIndex]];
  }

  return array;
}


function onGo() {

    // Clear the previous board
    document.getElementById("view").innerHTML = ""
    playersballs = {}



    console.log("Generating...");
    let players = parseInt(document.getElementById("players").value);
    console.log("Number of players:");
    console.log(players);
    let perplayer = Math.floor(15/players);
    console.log("Balls Per player:");
    console.log(perplayer);
    let shuffled = shuffle(balls);

    
    // Give each player their random balls
    let taken = 0;
    for (let i=0; i<players;i++) {
        playersballs[i] = balls.slice(taken, taken + perplayer);
        taken += perplayer;
    }
    console.log(playersballs)

    // Create div per player
    for (let i=0;i<players;i++) {
        // Player Div
        var playerdiv = document.createElement("div");
        var playerp = document.createElement("h3");
        var indicator = document.createElement("p");
        indicator.innerText = "•"
        indicator.classList.add("oindi");
        indicator.id = "i" + i;
        playerp.id = "p" + i;
        playerdiv.classList.add("playerdiv");
        playerp.innerText = "P" + (i+1);
        playerdiv.onclick = function() {toggleMe(i)};


        // Dice Div
        var dicediv = document.createElement("div");
        dicediv.id = "d" + i;
        dicediv.style.display = "none"
        dicediv.classList.add("dicediv");
        var balldiv;
        for (let j=0; j<playersballs[i].length; j++) {
            balldiv = document.createElement("img");
            balldiv.src = "./assets/ball_" + playersballs[i][j] + ".svg";
            dicediv.appendChild(balldiv);
        }




        playerdiv.appendChild(dicediv);
        playerdiv.appendChild(playerp);
        playerdiv.appendChild(indicator);
        document.getElementById("view").appendChild(playerdiv);
    }

}


function toggleMe(num) {
    console.log("Toggling ");
    console.log(num);
    if (document.getElementById("d" + num).style.display == "block") {
        document.getElementById("d" + num).style.display = "none";
        document.getElementById("p" + num).style.display = "block";
        document.getElementById("i" + num).classList.remove("indicator");
        setTimeout(() => {
            document.getElementById("i" + num).classList.add("indicator");
        }, 20);
    } else {
        document.getElementById("d" + num).style.display = "block";
        document.getElementById("p" + num).style.display = "none";
        document.getElementById("i" + num).style.opacity = "0";
    }


}
